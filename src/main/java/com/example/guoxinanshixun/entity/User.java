package com.example.guoxinanshixun.entity;

import io.swagger.models.auth.In;
import lombok.Data;

@Data
public class User {
    private Integer id;
    private String name;
    private String phone;
    private String pwd;
}
