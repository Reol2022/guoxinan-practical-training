package com.example.guoxinanshixun.entity.dto;

import lombok.Data;

@Data
public class UserRegisterDto {
    private String name;
    private String phone;
    private String pwd;
}
