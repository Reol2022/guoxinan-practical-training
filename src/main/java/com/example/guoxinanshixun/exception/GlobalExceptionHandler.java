package com.example.guoxinanshixun.exception;

import com.example.guoxinanshixun.common.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 全局异常处理(所有异常都执行)
     **/
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e) {
        e.printStackTrace();
        return Result.fail();
    }


    /**
     * 自定义异常处理（只有特定的地方会执行）
     **/
    @ExceptionHandler(BizException.class)
    public Result error(BizException e) {
        e.printStackTrace();
        return Result.fail();
    }

}

