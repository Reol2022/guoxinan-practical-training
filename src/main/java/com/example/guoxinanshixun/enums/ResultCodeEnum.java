package com.example.guoxinanshixun.enums;

import lombok.Getter;

@Getter
public enum  ResultCodeEnum {
    SUCCESS(200,"成功"),
    REGISTER_SUCCESS(201,"注册成功"),
    FAIL(202, "失败"),
    REGISTER_FAIL( 203, "注册失败"),
    SERVICE_ERROR(204, "服务异常"),
    DATA_ERROR(205, "数据异常"),

    LOGIN_AUTH(206, "未登陆"),
    PERMISSION(207, "没有权限"),

    CODE_ERROR(210, "验证码错误"),
    LOGIN_MOBLE_ERROR(211, "账号不正确"),
    REGISTER_MOBLE_ERROR(212, "手机号已存在"),
    LOGIN_AURH(213, "需要登录"),
    LOGIN_ACL(214, "没有权限"),

    URL_ENCODE_ERROR( 216, "URL编码失败"),
    ILLEGAL_CALLBACK_REQUEST_ERROR( 217, "非法回调请求"),
    FETCH_ACCESSTOKEN_FAILD( 218, "获取accessToken失败"),
    FETCH_USERINFO_ERROR( 219, "获取用户信息失败");


    private Integer code;
    private String message;

    private ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
