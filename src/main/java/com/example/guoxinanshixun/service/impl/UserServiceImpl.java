package com.example.guoxinanshixun.service.impl;

import com.example.guoxinanshixun.entity.User;
import com.example.guoxinanshixun.entity.dto.UserRegisterDto;
import com.example.guoxinanshixun.enums.ResultCodeEnum;
import com.example.guoxinanshixun.exception.BizException;
import com.example.guoxinanshixun.mapper.UserMapper;
import com.example.guoxinanshixun.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserMapper userMapper;

    /**
     * 注册接口
     * @param userRegDto
     * @return
     */
    @Override
    public User register(UserRegisterDto userRegDto){
        String phone = userRegDto.getPhone();
        //根据手机号查询用户
        User user = userMapper.selectUserByPhoneNumber(phone);
        //手机号不存在 抛出自定义异常
        if(user != null){
            throw new BizException(ResultCodeEnum.REGISTER_MOBLE_ERROR);
        }
        //注册
        User user1 = new User();
        user1.setName(userRegDto.getName());
        user1.setPwd(userRegDto.getPwd());
        user1.setPhone(userRegDto.getPhone());
        if (userMapper.insertUser(user1) == 0){
            throw new BizException(ResultCodeEnum.REGISTER_FAIL);
        }
        return user1;
    }
}
