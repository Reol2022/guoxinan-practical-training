package com.example.guoxinanshixun.service;

import com.example.guoxinanshixun.entity.User;
import com.example.guoxinanshixun.entity.dto.UserRegisterDto;

public interface UserService {
    /**
     * 注册接口
     * @param userRegDto
     * @return
     */
    User register(UserRegisterDto userRegDto);
}
