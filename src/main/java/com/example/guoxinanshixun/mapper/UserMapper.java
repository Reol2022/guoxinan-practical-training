package com.example.guoxinanshixun.mapper;

import com.example.guoxinanshixun.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * userMapper接口
 *
 * @author 蒼飞
 * @date 2024-06-12
 */
@Mapper
public interface UserMapper {
    /**
     * 根据手机号查询用户
     *
     */
    User selectUserByPhoneNumber(String phone);



    /**
     * 查询user
     *
     * @param id user主键
     * @return user
     */
    public User selectUserById(Long id);

    /**
     * 查询user列表
     *
     * @param user user
     * @return user集合
     */
    public List<User> selectUserList(User user);

    /**
     * 新增user
     *
     * @param user user
     * @return 结果
     */
     int insertUser(User user);

    /**
     * 修改user
     *
     * @param user user
     * @return 结果
     */
    public int updateUser(User user);

    /**
     * 删除user
     *
     * @param id user主键
     * @return 结果
     */
    public int deleteUserById(Long id);

    /**
     * 批量删除user
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserByIds(Long[] ids);
}
