package com.example.guoxinanshixun.controller;

import com.example.guoxinanshixun.common.Result;
import com.example.guoxinanshixun.entity.User;
import com.example.guoxinanshixun.entity.dto.UserRegisterDto;
import com.example.guoxinanshixun.enums.ResultCodeEnum;
import com.example.guoxinanshixun.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "用户接口")
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {


    private final UserService userService;

    @ApiOperation("注册")
    @PostMapping("/register")
    public Result register(@RequestBody UserRegisterDto userRegDto) {
        try{
            return Result.build(userService.register(userRegDto), ResultCodeEnum.REGISTER_SUCCESS);
        }catch (Exception e){
            return Result.fail(e.getMessage());
        }
    }
    @ApiOperation("测试")
    @GetMapping("/test")
    public String test() {
        return "123131231";
    }
}
