package com.example.guoxinanshixun;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan(basePackages = {"com.example"})
@MapperScan(basePackages ={"com.example.guoxinanshixun.mapper"} )
@SpringBootApplication
public class GuoxinanShixunApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuoxinanShixunApplication.class, args);
    }

}
